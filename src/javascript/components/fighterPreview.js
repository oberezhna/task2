import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  // debugger
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  // if(fighter) {
  const imgElement = createFighterImage(fighter);
  const fighterInfo = createFighterInfo(fighter);
  fighterElement.append(imgElement, fighterInfo);

  return fighterElement;
  // }
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterInfo(fighter){
  const { name, health, attack, defense } = fighter;
  
  const fighterInfoElement = createElement({
    tagName: 'div',
    className: `fighter-preview__info`
  });
  const fighterName = createElement({
    tagName: 'div',
    className: `fighter-preview__name`
  });
  fighterName.innerHTML=`<h2>Name: ${name}</h2>`;
  const fighterHealth = createElement({
    tagName: 'div',
    className: `fighter-preview__health`
  });
  fighterHealth.innerHTML= `<h3>Health: ${health}</h3>`;
  const fighterAttack = createElement({
    tagName: 'div',
    className: `fighter-preview__health`
  });
  fighterAttack.innerHTML= `<h3>Attack: ${attack}</h3>`;
  const fighterDefense = createElement({
    tagName: 'div',
    className: `fighter-preview__defense`
  });
  fighterDefense.innerHTML=`<h3>Defense: ${defense}</h3>`

  fighterInfoElement.append(fighterName, fighterHealth, fighterAttack, fighterDefense)

return fighterInfoElement
}
