import { controls } from '../../constants/controls';
import { fighters } from '../helpers/mockData';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    
    let comboMapOne = {'KeyQ': false, 'KeyW': false, 'KeyE': false}
    let comboMapTwo = {'KeyU': false, 'KeyI': false, 'KeyO': false}
    let critTime =  Date.now();
   
    document.addEventListener('keydown', function(event){
      console.log(event.code)
      console.log('comboMapOne', comboMapOne)
      console.log('comboMapTwo', comboMapTwo)
      if (event.code == controls.PlayerOneAttack && !event.repeat){
        if (event.code == controls.PlayerTwoBlock){
          return
        }
        getDamage(firstFighter, secondFighter);
      } else if (event.code == controls.PlayerTwoAttack && !event.repeat){
        if (event.code == controls.PlayerOneBlock){
          return
        }
        getDamage(secondFighter, firstFighter);
      } else if (event.code in comboMapOne) {
        comboMapOne[event.code] = true;
        if (comboMapOne['KeyQ'], comboMapOne['KeyW'], comboMapOne['KeyE']){
          getCriticalHitPower(firstFighter);
        }
      }  else if (event.code in comboMapTwo) {
        comboMapTwo[event.code] = true;
        if (comboMapTwo['KeyU'], comboMapTwo['KeyI'], comboMapTwo['KeyO']){
          getCriticalHitPower(secondFighter);
        }
      }
    });

    document.addEventListener('keyup', function(event){
      console.log('comboMapOne', comboMapOne)
      console.log('comboMapTwo', comboMapTwo)
      if (event.code in comboMapOne) {
        comboMapOne[event.code] = false;
      }  else if (event.code in comboMapTwo) {
        comboMapTwo[event.code] = false;
      }
    })
  });
}

export function getDamage(attacker, defender) {
  let hitPower = getHitPower(attacker);
  let blockPower = getBlockPower(defender);
  let damage = hitPower - blockPower;
  return (damage) ? damage : 0
}

export function getHitPower(fighter) {
  let criticalChance = getChance();
  let hitPower = fighter.attack*criticalChance;
  return hitPower 
}

export function getBlockPower(fighter) {
  let dodgeChance = getChance();
  let blockPower = fighter.defense*dodgeChance;
  return blockPower
}

export function getCriticalHitPower(fighter, critTime) {
  return critPossibility(critTime) ? fighter.attack*2 : 0 
}

function critPossibility(critTime){
  let currentTime = Date.now();
  if (currentTime - critTime > 10000) {
    critTime = Date.now();
    return true
  }
  return false 
}

function getChance(){
  return Math.random() + 1;
}
